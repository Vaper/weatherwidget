var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new mongoose.Schema({
    ip: {type: String, index: true},
    city: {type: String, index: true},
    country: {type: String, index: true},
    requests: [{type: ObjectId, ref: "Request"}]
});

var Model = mongoose.model('Visitor', schema);

module.exports = Model;