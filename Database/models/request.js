var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;

var schema = new mongoose.Schema({
    visitor: {type: ObjectId, ref: "Visitor"},
    city: {type: String, index: true},
    weather: {type: String, index: true},
    wid: Number,
    temperature: Number,
    pressure: {type: Number, index: true},
    wind: {coast: String, degree: Number, speed: Number},
    clouds: Number
});

var Model = mongoose.model('Request', schema);

module.exports = Model;