const mongoose = require("mongoose");
mongoose.Promise = require('bluebird');

const Visitor = require('./models/visitor');
const Request = require('./models/request');

var Database = function () {

    this.addVisitor = addVisitor;

    function init() {
        if (!Database.isConnected) {
            mongoose.connect('mongodb://localhost/weather', function (err) {
                if (err) {
                    console.log(err);
                    Database.isConnected = false;
                }

                Database.isConnected = true;
            });
        }
    }

    function addVisitor(visitorData) {
        if (!Database.isConnected) {
            return;
        }

        var savedVisitor;
        return Visitor.findOne({ip: visitorData.user.ip}).then(function (visitor) {
            if (!visitor || !visitor._id) {
                var visitor = new Visitor({
                    city: visitorData.user.city,
                    country: visitorData.user.country,
                    ip: visitorData.user.ip
                });

                return visitor.save()
            }

            return visitor;
        })
            .then(function (visitor) {
                delete visitorData['user'];
                var request = new Request(visitorData);
                request.visitor = visitor._id;
                savedVisitor = visitor;
                return request.save();
            })
            .then(function (visitorData) {
                savedVisitor.requests.push(visitorData._id);
                return savedVisitor.save();
            });
    }

    init();
};

Database.isConnected = false;

module.exports = Database;
