const request = require('request-promise');
const Database = require('../Database/db');
const _ = require('lodash');

/**
 * Simple class for the weather API
 * @author Ievgen Petrahshchuk a.k.a Vaper de kin 2016
 * @constructor
 */
var WeatherRequester = function (appID) {

    var db_;

    this.getWeather = getWeather;

    /**
     * Constructor, initiates the database
     */
    function init() {
        if (!Database.isConnected) {
            db_ = new Database();
        }
    }

    /**
     * Returns human-readable wind direction by degree
     * @param degree Floating point degree value
     * @returns {string}
     */
    function getWindDirectionByDegree(degree) {
        var directions = ['NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW',
            'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N'];

        for (var i = 0; i < directions.length; i++) {
            degree -= 22.5;
            if (degree < 0)
                return directions[i];
        }

        return directions[i];
    }


    /**
     * Handles user parameters, request the weather and returns the output to console
     */
    function getWeather(city, user) {
        return request({
            uri: "http://api.openweathermap.org/data/2.5/weather",
            qs: {appid: appID, q: city, units: "metric"}
        }).then(function (body) {
            try {
                body = JSON.parse(body);
            }
            catch (e) {
                throw(new Error("Object is not a valid JSON. API changed?"));
            }

            if (body.cod == 404) {
                throw(new Error("Your city was not found"));
            }
            else if (body.cod != 200) {
                return body.message;
            }
            else { //if code==200

                var returnObject = {};

                returnObject.city = body.name;

                if (body.weather.length) {
                    returnObject.weather = body.weather[0].description;
                    returnObject.id = body.weather[0].id;
                }

                if (body.main) {
                    returnObject.temperature = body.main.temp;
                    returnObject.pressure = body.main.pressure;
                }

                if (body.wind) {
                    returnObject.wind = {
                        coast: getWindDirectionByDegree(body.wind.deg),
                        degree: body.wind.deg,
                        speed: body.wind.speed
                    }
                }

                if (body.clouds)
                    returnObject.clouds = body.clouds.all;

                if (body.rain)
                    returnObject.rain = body.rain['3h'];

                if (body.snow)
                    returnObject.snow = body.snow['3h'];

                db_.addVisitor(_.extend(returnObject, {user: user}));

                return returnObject;
            }
        })
    }

    init();
};

module.exports = WeatherRequester;