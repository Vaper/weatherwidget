const WeatherRequester = require('./include/WeatherRequester');
const http = require('http');
const fs = require('fs-promise');
const jade = require('jade');
const cookieParser = require('cookie-parser');
const express = require('express');
const geoip = require('geoip-lite');

/**
 * Main server class
 * @author Ievgen Petrashchuk a.k.a Vaper de kin 2016
 * @constructor
 */
var Weather = function () {

    var weather;
    fs.readFile(__dirname + '/credentials.json')
        .then(function (credentials) {
            credentials = JSON.parse(credentials);
            weather = new WeatherRequester(credentials.appID);
        })
        .catch(function (err) {
            console.log(err);
        });

    var app = express();
    var server = http.createServer(app);

    //route functions

    /**
     * Index page handler. Renders the page
     * @param request
     * @param response
     * @param next
     */
    var index = function (request, response, next) {
        var fn = jade.compileFile(__dirname + "/view/index.jade");
        var body = fn();

        response.writeHead(200, {"Content-Type": "text/html"});
        response.end(body);
    }

    /**
     * Weather handler, makes request through API
     * @param request
     * @param response
     * @param next
     */
    function getWeather(request, response, next) {
        var user = {};
        if (request.cookies.visitor) {
            user = JSON.parse(request.cookies.visitor);
        }
        weather.getWeather(request.cookies.city, user)
            .then(function (data) {
                response.writeHead(200, {"Content-Type": "application/json"});
                response.end(JSON.stringify(data));
            })
            .catch(function (err) {
                console.log(err);
                response.writeHead(500, {"Content-Type": "application/json"});
                response.end(JSON.stringify({error: 1}));
            });
    }

    //parsing coockies
    app.use(cookieParser());

    //setting the charset of js files
    app.use(function (req, res, next) {
        if (/.*\.js/.test(req.path)) {
            res.charset = "utf-8";
        }
        next();
    });

    //determining user  city
    app.use(function (request, response, next) {
        var city = request.cookies.city;

        if (request.query.city)
            city = request.query.city;

        if (!city) {
            var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
            var geo = geoip.lookup(ip);

            //saving geolocation data
            if (geo) {
                response.cookie('city', geo.city.toLowerCase(), {maxAge: "31540000000"});
            }
            else {
                response.cookie('city', 'San Francisco', {maxAge: "31540000000"});
            }
        }
        else {
            response.cookie('city', city, {maxAge: "31540000000"});
        }

        //setting the visitor parameters
        if (!request.cookies['visitor']) {
            if (geo) {
                response.cookie('visitor', JSON.stringify({
                    country: geo.country,
                    city: geo.city,
                    ip: request.ip
                }), {maxAge: "31540000000"});
            }
            else {
                response.cookie('visitor', JSON.stringify({
                    country: "Unknown",
                    city: "Unknown",
                    ip: request.ip
                }), {maxAge: "31540000000"});
            }
        }

        next();
    });


    //main routes
    app.all('/', index);
    app.all('/weather', getWeather);

    //handling static files
    app.use(express.static('static'));

    //handling 404
    app.use(function (request, response, next) {

        var fn = jade.compileFile(__dirname + "/view/err404.jade");
        var body = fn();

        response.writeHead(404, {"Content-Type": "text/html"});
        response.end(body);
    });


    server.listen(8080);

    console.log("Weather server started");
};


var weather = new Weather();


/**
 * Just handling ctrl+C to exit server properly
 */
process.on('SIGINT', function () {
    console.log("Weather server stopped");
    process.exit();
});
