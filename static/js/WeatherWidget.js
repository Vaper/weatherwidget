/**
 * Weatther widget. Cna be built in the page by selector.
 * Gets info from server and updates every 10 minutes
 * @author Ievgen Petrashchuk a.k.a Vaper de kin 2016
 * @param selector CSS selector of the element, where widget whould be built in
 * @constructor
 */
var WeatherWidget = function (selector) {

    var city = $.cookie('city');

    var codes = {
        200: "11", 201: "11", 202: "11", 210: "11", 211: "11", 221: "11", 230: "11", 231: "11", 232: "11", 300: "09",
        301: "09", 302: "09", 310: "09", 311: "09", 312: "09", 313: "09", 314: "09", 321: "09", 500: "10", 501: "10",
        502: "10", 503: "10", 504: "10", 511: "13", 520: "09", 521: "09", 522: "09", 531: "09", 600: "13", 601: "13",
        602: "13", 611: "13", 612: "13", 615: "13", 616: "13", 620: "13", 621: "13", 622: "13", 701: "50", 711: "50",
        721: "50", 731: "50", 741: "50", 751: "50", 761: "50", 762: "50", 771: "50", 781: "50", 800: "01", 801: "02",
        802: "03", 803: "03", 804: "04"
    }

    var html = '  <div class="WWidget">\
                    <div class="WHeader">Current weather in ' + city + '</div>\
                    <div class="MainWeather">\
                        <div class="TemperatureContainer">\
                            <div class="TemperatureImage"><img /></div>\
                            <div class="Temperature"></div>\
                            <div class="WDescription"></div>\
                        </div>\
                        <div class="WPressure"></div>\
                        <div class="Wind">\
                            <div class="WindContainer">\
                                <div class="WindDirection"></div>\
                                <div class="WindCompass"><div class="WindCompasDirection"></div></div>\
                            </div>\
                        </div>\
                        <div class="Clouds hidden"></div>\
                        <div class="Rain hidden"></div>\
                        <div class="Snow hidden"></div>\
                    </div>\
                </div>';

    $(selector).html(html);

    /**
     * Updates weather data from server and fill in Widget's html
     */
    var update = function () {
        $.ajax({url: "/weather"}).done(function (data) {
            $('.WHeader').html('Current weather in ' + data.city);
            $('.Temperature').html(data.temperature + ' &deg;');
            $('.TemperatureImage img').attr("src", "http://openweathermap.org/img/w/" + codes[data.id] + "d.png");
            $('.WDescription').html(data.weather);
            $('.WPressure').html("Atmospheric pressure: " + data.pressure);
            $('.WindDirection').html("Wind<br>" + data.wind.speed + " m/s " + (data.wind.coast || ''));
            $('.WindCompasDirection').css({transform: "rotate(" + data.wind.degree + "deg)"});

            if (data.clouds)
                $('.Clouds').removeClass('hidden').html('Clouds: ' + data.clouds + ' %');
            else
                $('.Cloud').addClass('hidden');

            if (data.snow)
                $('.Snow').removeClass('hidden').html('Snow: ' + data.snow + ' mm');
            else
                $('.Snow').addClass('hidden');

            if (data.rain)
                $('.Rain').removeClass('hidden').html('Rain: ' + data.rain + ' mm');
            else
                $('.Rain').addClass('hidden');
        });
    };

    update();

    setInterval(update, 600000);//update every 10 minutes
}